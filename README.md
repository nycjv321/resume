# Javier's résumé

![build status](https://gitlab.com/nycjv321/resume/badges/master/build.svg)

This repo holds my resume in code form.

## Viewing My Resume

* See "Building It Using Docker" below
* The build process will make the latest version of the resume accessible [here](http://resume.nycjv321.com.s3-website-us-east-1.amazonaws.com/)
* Download the pdf artifact from a recent pipeline [here](https://gitlab.com/nycjv321/resume/pipelines?scope=all&page=1)

## Technical Details

My resume is written using [LaTeX](https://www.latex-project.org/).

## Building it using docker

    docker run --rm -v $(pwd):/latex tianon/latex pdflatex -output-directory latex  ./latex/javier-resume.tex
